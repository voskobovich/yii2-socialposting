<?php
/**
 * Created by PhpStorm.
 * User: Vitaly Voskobovich
 * Date: 10.06.14 16:03
 */
namespace voskobovich\socialposting;

class Collection extends \yii\base\Component
{
	/**
	 * @var array list of social clients with their configuration in format: 'clientId' => [...]
	 */
	private $_clients = [];

	/**
	 * @param array $clients list of social clients.
	 */
	public function setClients(array $clients)
	{
		$this->_clients = $clients;
	}

	/**
	 * @return ClientInterface[] list of social clients.
	 */
	public function getClients()
	{
		$clients = [];
		foreach ($this->_clients as $id => $client) {
			$clients[$id] = $this->getClient($id);
		}

		return $clients;
	}

	/**
	 * @param string $id service id.
	 * @return ClientInterface social client instance.
	 * @throws InvalidParamException on non existing client request.
	 */
	public function getClient($id)
	{
		if (!array_key_exists($id, $this->_clients)) {
			throw new InvalidParamException("Unknown social client '{$id}'.");
		}
		if (!is_object($this->_clients[$id])) {
			$this->_clients[$id] = $this->createClient($this->_clients[$id]);
		}

		return $this->_clients[$id];
	}

	/**
	 * Checks if client exists in the hub.
	 * @param string $id client id.
	 * @return boolean whether client exist.
	 */
	public function hasClient($id)
	{
		return array_key_exists($id, $this->_clients);
	}

	/**
	 * Creates social client instance from its array configuration.
	 * @param array $config social client instance configuration.
	 * @return ClientInterface social client instance.
	 */
	protected function createClient($config)
	{
		return \Yii::createObject($config);
	}

	/**
	 * Отправка сообщения конкретному клиенту
	 * @param $client
	 * @param $message
	 */
	public function send($client, $message)
	{
		return $this->clients[$client]->sendPost($message);
	}

	/**
	 * Отправка всем
	 * @param $message
	 */
	public function sendToAll($message)
	{
		foreach($this->clients as $client)
		{
			echo $client->sendPost($message);
			echo "-----";
		}
	}
}