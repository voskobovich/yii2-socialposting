<?php
/**
 * Created by PhpStorm.
 * User: Vitaly Voskobovich
 * Date: 10.06.14 16:03
 */
namespace voskobovich\socialposting\clients;

use Yii;
use \voskobovich\socialposting\lib\twitter\TwitterOAuth;

/**
 * Example application configuration:
 *
 * ~~~
 * 'components' => [
 *     'socialPostingCollection' => [
 *         'class' => 'voskobovich\socialposting\Collection',
 *         'clients' => [
 *             'twitter' => [
 *                 'class' => 'voskobovich\socialposting\clients\Twitter',
 *                 'clientId' => 'twitter_client_id',
 *                 'clientSecret' => 'twitter_client_secret',
 *             ],
 *         ],
 *     ]
 *     ...
 * ]
 * ~~~
 *
 * @author Vitaly Voskobovich <raficone@gmail.com>
 * @since 2.0
 */
class Twitter extends \yii\base\Component implements \voskobovich\socialposting\ClientInterface
{
	/**
	 * @var string
	 */
	public $apiKey = '';

	/**
	 * @var string
	 */
	public $apiSecret = '';

	/**
	 * @var string
	 */
	public $oauthToken = '';

	/**
	 * @var string
	 */
	public $oauthSecret = '';

	/**
	 * Экземпляр класса Api
	 * @var null
	 */
	private $apiClass = NULL;

	/**
	 * Пишем ошибки в логи
	 * @param $errorData
	 * @param string $category
	 */
	private function log($errorData, $category)
	{
		$message = print_r($errorData, true);
		Yii::error($message, $category);
	}

	/**
	 * Инициализация компонента
	 */
	public function init()
	{
		$this->apiClass = new TwitterOAuth(
			$this->apiKey,
			$this->apiSecret,
			$this->oauthToken,
			$this->oauthSecret
		);
	}

	/**
	 * Публикация записи
	 */
	public function sendPost($message)
	{
		if(empty($message))
			return false;
		$response = $this->apiClass->post('statuses/update', array('status' => $message));

		if(!empty($response->error) || !empty($response->errors))
			$this->log($response, 'twitter.sendPost');
		else
			return $response->id;

		return false;
	}
}