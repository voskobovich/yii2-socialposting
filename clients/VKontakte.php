<?php
/**
 * Created by PhpStorm.
 * User: Vitaly Voskobovich
 * Date: 10.06.14 16:03
 */
namespace voskobovich\socialposting\clients;

use Yii;
use yii\helpers\Json;

/**
 * Example application configuration:
 *
 * ~~~
 * 'components' => [
 *     'socialPostingCollection' => [
 *         'class' => 'voskobovich\socialposting\Collection',
 *         'clients' => [
 *             'vkontakte' => [
 *                 'class' => 'voskobovich\socialposting\clients\VKontakte',
 *                 'clientId' => 'vkontakte_client_id',
 *                 'clientSecret' => 'vkontakte_client_secret',
 *             ],
 *         ],
 *     ]
 *     ...
 * ]
 * ~~~
 *
 * @author Vitaly Voskobovich <raficone@gmail.com>
 * @since 2.0
 */
class VKontakte extends \yii\base\Component implements \voskobovich\socialposting\ClientInterface
{
	const API_SERVER = 'https://api.vk.com/method/';
	const CALLBACK_BLANK = 'https://oauth.vk.com/blank.html';
	const AUTHORIZE_URL = 'https://oauth.vk.com/authorize?client_id={app_id}&scope={scope}&redirect_uri={redirect_uri}&display={display}&v={version}&response_type={response_type}';

	/**
	 * Версия API
	 * @var string
	 */
	public $version = '5.21';

	/**
	 * Ограничение прав доступа
	 * Полные права: 2015231
	 * @var array
	 */
	public $scope = ['wall','offline'];

	/**
	 * Идентификатор приложения
	 * @var string
	 */
	public $appId;

	/**
	 * Идентификатор группы
	 * @var string
	 */
	public $groupId;

	/**
	 * Токен доступа
	 * @var string
	 */
	private $access_token = '';

	/**
	 * Инициализация Api метода
	 * @param string $method - название метода <http://vk.com/dev/methods>
	 * @param array $vars - параметры метода
	 * @return array - выводит массив данных или ошибку
	 * @param $method
	 * @param array $vars
	 *
	 * @return array
	 * @throws CException
	 */
	private function api($method, array $vars = [])
	{
		if(!empty($this->access_token))
			$vars['access_token'] = $this->access_token;

		$params = http_build_query($vars);

		$url = self::API_SERVER . "{$method}?{$params}&v={$this->version}";

		return (array)$this->call($url);
	}

	/**
	 * Выполнение запроса на сервер VK Api
	 * @param string $url
	 *
	 * @return bool|mixed|string
	 */
	private function call($url)
	{
		$json = function_exists('curl_init') ? $this->curl_post($url) : file_get_contents($url);
		$json = Json::decode($json, true);

		if(isset($json['response']))
			return $json['response'];

		return $json;
	}

	/**
	 * Отправляет запрос через CURL
	 * @param $url
	 *
	 * @return bool|mixed
	 */
	private function curl_post($url)
	{
		$param = parse_url($url);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $param['scheme'].'://'.$param['host'].$param['path']);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $param['query']);
		$out = curl_exec($curl);

		curl_close($curl);

		return $out;
	}

	/**
	 * Пишем ошибки в логи
	 * @param $errorData
	 * @param string $category
	 */
	private function log($errorData, $category)
	{
		$message = print_r($errorData, true);
		Yii::error($message, $category);
	}

	/**
	 * Сеттер токена доступа
	 * @param $token
	 */
	public function setAppAccessToken($token)
	{
		$this->access_token = $token;
	}

	/**
	 * Получение токена доступа для приложения
	 *
	 * @return mixed
	 */
	public function getAccessTokenUrl()
	{
		$scope = implode(',', $this->scope);
		$params = [
			'client_id' => $this->appId,
			'scope' => $scope,
			'redirect_uri' => self::CALLBACK_BLANK,
			'display' => 'page',
			'v' => $this->version,
			'response_type' => 'token'
		];

		$params = http_build_query($params);

		return "https://oauth.vk.com/authorize/?{$params}";
	}

	/**
	 * Публикация поста на стену
	 * @param $message
	 *
	 * @return bool
	 */
	public function sendPost($message)
	{
		$params = ['message'=>$message];
		if(!empty($this->groupId))
			$params['owner_id'] = 0 - (int) $this->groupId;

		$response = $this->api('wall.post', $params);

		if(!empty($response['error']))
			$this->log($response['error'], 'vkontakte.sendPost');
		else
			return !empty($response['post_id']) ? $response['post_id'] : false;

		return false;
	}
}