<?php
/**
 * Created by PhpStorm.
 * User: Vitaly Voskobovich
 * Date: 10.06.14 16:03
 */
namespace voskobovich\socialposting\clients;

use Yii;
use voskobovich\socialposting\lib\facebook\FacebookApi;

/**
 * Example application configuration:
 *
 * ~~~
 * 'components' => [
 *     'socialPostingCollection' => [
 *         'class' => 'voskobovich\socialposting\Collection',
 *         'clients' => [
 *             'facebook' => [
 *                 'class' => 'voskobovich\socialposting\clients\Facebook',
 *                 'clientId' => 'facebook_client_id',
 *                 'clientSecret' => 'facebook_client_secret',
 *             ],
 *         ],
 *     ]
 *     ...
 * ]
 * ~~~
 *
 * @author Vitaly Voskobovich <raficone@gmail.com>
 * @since 2.0
 */
class Facebook extends \yii\base\Component implements \voskobovich\socialposting\ClientInterface
{
	/**
	 * Идентификатор приложения.
	 * Выдается при создании standalone приложения.
	 * @var string
	 */
	public $appId = '';

	/**
	 * Секретный ключь приложения.
	 * Выдается при создании standalone приложения.
	 * @var string
	 */
	public $appSecret = '';

	/**
	 * Идентификатор группы
	 * @var string
	 */
	public $groupId = '';

	/**
	 * Защищенный ключ приложения.
	 * Выдается при создании standalone приложения.
	 * @var string
	 */
	private $access_token = '';

	/**
	 * Экземпляр класса VKApi
	 * @var null
	 */
	private $apiClass = NULL;

	/**
	 * Пишем ошибки в логи
	 * @param $errorData
	 * @param string $category
	 */
	private function log($errorData, $category)
	{
		$message = print_r($errorData, true);
		Yii::error($message, $category);
	}

	/**
	 * Инициализация компонента
	 */
	public function init()
	{
		$this->apiClass = new FacebookApi([
			'appId'  => $this->appId,
			'secret' => $this->appSecret,
			'cookie' => true,
		]);

		if(!empty($this->access_token)) {
			$this->apiClass->setAccessToken($this->access_token);
		} else {

			echo $this->apiClass->getLoginUrl(['scope' => 'publish_stream,manage_pages,publish_actions']);
			echo "<br>";

			$access_token = $this->apiClass->getAccessToken();
			echo "Access Token: {$access_token}";
			exit;
		}
	}

	/**
	 * Сеттер токена доступа
	 * @param $token
	 */
	public function setAppAccessToken($token)
	{
		$this->access_token = $token;
	}

	/**
	 * Публикация записи
	 */
	public function sendPost($message, $link = '')
	{
		try {
			$response = $this->apiClass->api("/{$this->groupId}/feed/", "POST", [
				'message' => $message,
				'link' => $link
			]);

			if(!empty($response['error']))
				$this->log($response, 'facebook.sendPost');
			else
				return $response['id'];

		} catch(Exception $e) {
			$this->log($e, 'facebook.sendPost');
		}

		return false;
	}
}