<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace voskobovich\socialposting;

/**
 * ClientInterface declares basic interface all Auth clients should follow.
 *
 * @author Paul Klimov <klimov.paul@gmail.com>
 * @since 2.0
 */
interface ClientInterface
{
	/**
	 * @param $message
	 * @return string post id
	 */
	public function sendPost($message);
}
