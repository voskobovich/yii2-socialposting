Yii2 Social Posting
===================
Implements posting records in social networks

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist https://bitbucket.org/voskobovich/yii2-socialposting "*"
```

or add

```
"https://bitbucket.org/voskobovich/yii2-socialposting": "*"
```

to the require section of your `composer.json` file.