<?php
/**
 * Created by PhpStorm.
 * Author: Vitaly Voskobovich
 * Email: vitaly@voskobovich.com
 * Date: 12.06.14 18:32
 */

namespace voskobovich\socialposting\lib\twitter;

class OAuthConsumer {
	public $key;
	public $secret;

	function __construct($key, $secret, $callback_url=NULL) {
		$this->key = $key;
		$this->secret = $secret;
		$this->callback_url = $callback_url;
	}

	function __toString() {
		return "OAuthConsumer[key=$this->key,secret=$this->secret]";
	}
}